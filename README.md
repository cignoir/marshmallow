# Getting Started
1. ``$ git clone git@github.com:cignoir/marshmallow.git``
2. DocumentRootを ``public`` ディレクトリに設定する。
3. php.iniでfileinfo.dllを有効にする。
4. ローカルで Apache とMySQL を起動する
5. MySQL上でデータベースを作成する。
6. FuelPHP用のDB接続情報を設定する。設定するファイルは ``fuel/app/config`` 以下にある。
7. 次のコマンドでテーブルを自動生成する。 ``php oil r migrate:up --default -all``
8. ローカルサーバを起動する。 ``php -S localhost:8000 -t public``
9. ブラウザで http://localhost:8000 にアクセスして起動を確認する。
