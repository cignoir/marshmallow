<?php if ($news): ?>
    <table class="table table-striped">
        <?php foreach ($news as $item): ?>
        <tr>
            <td><p class="text-right"><?=$item->created_at?></p></td>
            <td>
                <span class="tweet-subject"><?=$item->subject?></span><br>
                <pre><?=$item->content?></pre>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php else: ?>
    <p>ニュースがありません</p>
<?php endif; ?><p>
</p>
