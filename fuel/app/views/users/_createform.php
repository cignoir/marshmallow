<?php if ($user->image) { echo "<img src=\"" . Uri::create("usericon/" . $user->image). "\" width=200 height=200>"; } ?>

<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('ユーザーID', 'username', array('class'=>'control-label')); ?>

				<?php echo Form::input('username', Input::post('username', isset($user) ? $user->username : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Username')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('パスワード', 'password', array('class'=>'control-label')); ?>

				<?php echo Form::input('password', Input::post('password', isset($user) ? $user->password : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Password')); ?>

		</div>
                <div class="form-group">
                    <?php echo Form::label('氏名', 'username', array('class'=>'control-label')); ?>
                    <?php echo Form::input('fullname', Input::post('fullname', isset($user) ? $user->username : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Fullname')); ?>
		</div>
		<div class="form-group">
			<?php echo Form::label('メールアドレス', 'email', array('class'=>'control-label')); ?>

				<?php echo Form::input('email', Input::post('email', isset($user) ? $user->email : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Email')); ?>

		</div>
                <div class="form-group">
                    <input type="hidden" name="MAX_FILE_SIZE" value="30000" class="form-control"/>
                    プロフィール画像をアップロード: <input name="usericon" type="file" class="form-control"/>
                </div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', '登録する', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>