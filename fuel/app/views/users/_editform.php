<?php if ($user->image) { echo "<img src=\"" . Uri::create("usericon/" . $user->image). "\" width=200 height=200>"; } ?>
<br>
<?php echo Form::open(array("class"=>"form-horizontal", "enctype"=>'multipart/form-data')); ?>
    <fieldset>
        <div class="form-group">
            <?php echo Form::label('ユーザーID', 'username', array('class'=>'control-label')); ?>
            <?php echo Form::input('username', Input::post('username', isset($user) ? $user->username : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Username')); ?>
        </div>
        <div class="form-group">
            <?php echo Form::label('氏名', 'fullname', array('class'=>'control-label')); ?>
            <?php echo Form::input('fullname', Input::post('fullname', isset($user) ? $user->fullname : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Fullname')); ?>
        </div>
        <div class="form-group">
            <?php echo Form::label('メールアドレス', 'email', array('class'=>'control-label')); ?>
            <?php echo Form::input('email', Input::post('email', isset($user) ? $user->email : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Email')); ?>
        </div>
        <div class="form-group">
            <?php echo Form::label('プロフィール', 'description', array('class'=>'control-label')); ?>
            <?php echo Form::textarea('description', Input::post('description', isset($user) ? $user->description : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Description')); ?>
        </div>
        <div class="form-group">
            <?php echo Form::file('usericon'); ?>
        </div>
        <div class="form-group">
            <label class='control-label'>&nbsp;</label>
            <?php echo Form::submit('submit', '更新する', array('class' => 'btn btn-primary')); ?>
        </div>
    </fieldset>
<?php echo Form::close(); ?>
