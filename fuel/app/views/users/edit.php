<h2><i class="glyphicon glyphicon-pencil"></i> 編集</h2>
<hr>
<?php echo Html::anchor('users/view/'.$user->id, '<i class="glyphicon glyphicon-eye-open"></i>  閲覧に戻る', array('class' => 'btn btn-default')); ?>
<hr>
<?php echo render('users/_editform'); ?>
