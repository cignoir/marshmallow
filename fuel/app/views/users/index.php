<h2><i class="glyphicon glyphicon-list"></i> 一覧</h2>
<br>
<?php if ($users): ?>
<table class="table table-striped">
    <thead>
        <tr>
            <th>ユーザーID</th>
            <th>氏名</th>
            <th>グループ</th>
            <th>メールアドレス</th>
            <th>登録日時</th>
            <th>更新日時</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $item): ?>
        <tr>
            <td><?php echo $item->username; ?></td>
            <td><?php echo $item->fullname; ?></td>
            <td><?php echo $item->group; ?></td>
            <td><?php echo $item->email; ?></td>
            <td><?php echo $item->created_at; ?></td>
            <td><?php echo $item->updated_at; ?></td>
            <td>
                <div class="btn-toolbar">
                    <?php echo Html::anchor('users/view/'.$item->id, '<i class="glyphicon glyphicon-eye-open"></i>', array('class' => 'btn btn-default')); ?>
                    <?php echo Html::anchor('users/edit/'.$item->id, '<i class="glyphicon glyphicon-pencil"></i>', array('class' => 'btn btn-success')); ?>
                    <?php echo Html::anchor('users/delete/'.$item->id, '<i class="glyphicon glyphicon-remove"></i>', array('class' => 'btn btn-danger', 'onclick' => "return confirm('本当に削除してもよろしいですか？')")); ?>
                </div>
            </td>
        </tr>
        <?php endforeach; ?>	
    </tbody>
</table>

<?php else: ?>
<p>No Users.</p>

<?php endif; ?>
<p>
    <?php echo Html::anchor('users/create', '<i class="glyphicon glyphicon-plus"></i> 新規登録', array('class' => 'btn btn-success')); ?>
</p>
