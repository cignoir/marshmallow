<p class="text-right text-info">ようこそ、<?=$me->fullname?> さん <?php echo Html::anchor('home', '<i class="glyphicon glyphicon-refresh"></i>');?></p>
<br>
<div class="col-md-12">
<h3 class="label label-warning">最新ニュース</h3>
<table class="table table-striped">
    <?php foreach ($news as $item): ?>
    <tr>
        <td class="tweet-subject"><?=$item->created_at?></td>
        <td><span class="news">New!</span></td>
        <td>
            <span class="tweet-subject"><?=$item->subject?></span><br>
            <pre><?=$item->content?></pre>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
</div>

<div class="col-md-9">
<h3 class="label label-primary">最新記事</h3>
<table class="table table-striped">
    <?php foreach ($articles as $item): ?>
    <tr>
        <td><?php if ($item->user->image) { echo "<img src=\"" . Uri::create("usericon/" . $item->user->username . '/' . $item->user->image). "\" width=60 height=60>"; } ?></td>
        <td class="tweet-name"><?php echo $item->user->fullname ? $item->user->fullname : $item->username; ?></td>
        <td>
            <span class="tweet-subject"><?=$item->subject?></span><br>
            <?php if(mb_strlen($item->subject . $item->content) > 70) { echo mb_substr($item->content, 0, 70) . ' ... ' . Html::anchor('articles/view/'.$item->id, '<i class="glyphicon glyphicon-chevron-right"></i> 続きを読む');} else { echo $item->content; } ?><br>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
</div>

<div class="col-md-3">
<h3 class="label label-success">新規ユーザー</h3>
<table class="table table-striped">
    <?php foreach ($users as $item): ?>
    <tr>
        <td><?php echo $item->fullname ? $item->fullname : $item->username; ?></td>
        <td><?php if ($item->image) { echo "<img src=\"" . Uri::create("usericon/" . $item->username . '/' . $item->image). "\" width=60 height=60>"; } ?></td>
    </tr>
    <?php endforeach; ?>
</table>
</div>