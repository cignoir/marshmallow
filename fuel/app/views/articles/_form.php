<?php echo Form::open(array("class"=>"form-horizontal")); ?>

    <fieldset>
        <div class="form-group">
            <?php echo Form::label('タイトル(空欄可)', 'subject', array('class'=>'control-label')); ?>
            <?php echo Form::input('subject', Input::post('subject', isset($article) ? $article->subject : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Subject')); ?>
        </div>
        <div class="form-group">
            <?php echo Form::label('内容', 'content', array('class'=>'control-label')); ?>
            <?php echo Form::textarea('content', Input::post('content', isset($article) ? $article->content : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Content')); ?>
        </div>
        <div class="form-group">
            <label class='control-label'>&nbsp;</label>
            <?php echo Form::submit('submit', '投稿する', array('class' => 'btn btn-primary')); ?>		</div>
    </fieldset>
<?php echo Form::close(); ?>