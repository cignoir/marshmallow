<p class="text-right">
    <?php echo Html::anchor('articles/create', '<i class="glyphicon glyphicon-plus"></i> 新規投稿', array('class' => 'btn btn-success')); ?>
</p>
<?php if ($articles): ?>
    <table class="table table-striped">
        <?php foreach ($articles as $item): ?>
        <tr>
            <td><?php if ($item->user->image) { echo "<img src=\"" . Uri::create("usericon/" . $item->user->username . '/' . $item->user->image). "\" width=60 height=60>"; } ?></td>
            <td class="tweet-name"><?php echo $item->user->fullname ? $item->user->fullname : $item->username; ?></td>
            <td>
                <span class="tweet-subject"><?=$item->subject?></span><br>
                <?php if(mb_strlen($item->subject . $item->content) > 100) { echo mb_substr($item->content, 0, 100) . ' ... ' . Html::anchor('articles/view/'.$item->id, '<i class="glyphicon glyphicon-chevron-right"></i> 続きを読む');} else { echo $item->content; } ?><br>
                <p class="text-right"><?=$item->created_at?></p>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php else: ?>
    <p>記事がありません</p>
<?php endif; ?><p>
</p>
