<?php if ($flashinfos): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Subject</th>
			<th>Content</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($flashinfos as $item): ?>
            <tr>
                <td><?php echo $item->subject; ?></td>
                <td><?php echo $item->content; ?></td>
                <td>
                    <div class="btn-toolbar">
                        <div class="btn-group">
                            <?php echo Html::anchor('flashinfos/view/'.$item->id, '<i class="glyphicon glyphicon-eye-open"></i>', array('class' => 'btn btn-default')); ?>
                            <?php echo Html::anchor('flashinfos/edit/'.$item->id, '<i class="glyphicon glyphicon-pencil"></i>', array('class' => 'btn btn-success')); ?>
                            <?php echo Html::anchor('flashinfos/delete/'.$item->id, '<i class="glyphicon glyphicon-remove"></i>', array('class' => 'btn btn-small btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>
                        </div>
                    </div>

                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
</table>

<?php else: ?>
<p>No Flashinfos.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('flashinfos/create', '<i class="glyphicon glyphicon-plus"></i> 新規登録', array('class' => 'btn btn-success')); ?>

</p>
