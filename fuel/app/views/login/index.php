<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>BestCall+ ログイン画面</title>
    <?php echo Asset::css('bootstrap.css'); ?>
</head>
<body>
<div class="container">
    <div class="col-md-12">
        <h1>BestCall+ ログイン</h1>
        
        <?php if (isset($error)): ?>
        <?php echo $error ?>
        <?php endif ?>

        <?php echo $form ?>
    </div>
</div>
</body>
</html>