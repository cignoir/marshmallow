<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <?php echo Asset::css('bootstrap.css'); ?>
    <?php echo Asset::css('template.css'); ?>
    <?php echo Asset::css('dashboard.css'); ?>
    <style>
            body { margin: 40px; }
    </style>
</head>
<body>
    <!-- ヘッダー -->
    <div class="navbar navbar-fixed-top header" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <?php echo Html::anchor('home', 'BestCall+', array('class' => 'navbar-brand')); ?>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><?php echo Html::anchor('home', 'ダッシュボード', array('class' => 'whitetext')); ?></li>
            <li><?php echo Html::anchor('userlist/edit/' . Auth::get_user_id()[1], 'マイページ', array('class' => 'whitetext')); ?></li>
            <li><?php echo Html::anchor('logout', 'ログアウト', array('class' => 'whitetext')); ?></li>
          </ul>
        </div>
    </div>
    
    <div class="container-fluid">
        <div class="row">
            <!-- サイドバー -->
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                  <li>▼ コンテンツ</li>
                  <li class="<?php if ($title=="ダッシュボード"): ?>active<?php endif; ?>"><?php echo Html::anchor('home', '<i class="glyphicon glyphicon-home"></i> ダッシュボード'); ?></li>
                  <li class="<?php if ($title=="ニュース"): ?>active<?php endif; ?>"><?php echo Html::anchor('news', '<i class="glyphicon glyphicon-flash"></i> ニュース'); ?></li>
                  <li class="<?php if ($title=="社員一覧"): ?>active<?php endif; ?>"><?php echo Html::anchor('userlist', '<i class="glyphicon glyphicon-user"></i> 社員一覧'); ?></li>
                  <li class="<?php if ($title=="記事一覧"): ?>active<?php endif; ?>"><?php echo Html::anchor('articles', '<i class="glyphicon glyphicon-book"></i> 記事一覧'); ?></li>
                  <li class="<?php if ($title=="ランキング"): ?>active<?php endif; ?>"><a href="#"><i class="glyphicon glyphicon-bookmark"></i> ランキング</a></li>
                  <li class="<?php if ($title=="ご意見箱"): ?>active<?php endif; ?>"><a href="#"><i class="glyphicon glyphicon-envelope"></i> ご意見箱</a></li>
                  <li>▼ 管理画面</li>
                  <li class="<?php if ($title=="ニュース管理"): ?>active<?php endif; ?>"><?php echo Html::anchor('flashinfos', '<i class="glyphicon glyphicon-flash"></i> ニュース管理'); ?></li>
                  <li class="<?php if ($title=="社員管理"): ?>active<?php endif; ?>"><?php echo Html::anchor('users', '<i class="glyphicon glyphicon-user"></i> 社員管理'); ?></li>
                </ul>
              </div>

            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <div class="col-md-10">
                    <?php if (Session::get_flash('success')): ?>
                    <div class="alert alert-success">
                        <strong>Success</strong>
                        <p>
                        <?php echo implode('</p><p>', e((array) Session::get_flash('success'))); ?>
                        </p>
                    </div>
                    <?php endif; ?>

                    <?php if (Session::get_flash('error')): ?>
                    <div class="alert alert-error">
                        <strong>Error</strong>
                        <p>
                        <?php echo implode('</p><p>', e((array) Session::get_flash('error'))); ?>
                        </p>
                    </div>
                    <?php endif; ?>
                </div>

                <!-- メインコンテンツ -->
                <div class="col-md-10">
                    <?php echo $content; ?>
                </div>

                <!-- フッター 
                <footer>
                    Copyright © 2014 BestEffort+ All Rights Reserved.
                </footer>
                -->
            </div>
        </div>
    </div>
</body>
</html>
