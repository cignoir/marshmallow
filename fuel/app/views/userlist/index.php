<?php if ($users): ?>
    <table class="table table-striped">
        <?php foreach ($users as $user): ?>
        <tr>
            <td style="height:60px;line-height: 60px;"><?php if ($user->image) { echo "<img src=\"" . Uri::create("usericon/" . $user->username . '/' . $user->image). "\" width=60 height=60>"; } ?></td>
            <td style="height:60px;line-height: 60px;">
                <?php echo Html::anchor('userlist/view/' . $user->id, $user->fullname ? $user->fullname : $user->username); ?>
            </td>
            <td style="height:60px;line-height: 60px;"><?php if ($user->description) { echo "<pre>" . $user->description . "</pre>"; } ?></td>
            <td style="height:60px;line-height: 60px;"><?php echo Html::anchor('userlist/view/'.$user->id, '<i class="glyphicon glyphicon-eye-open"></i>', array('class' => 'btn btn-default')); ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php else: ?>
<p>No Users.</p>

<?php endif; ?>
