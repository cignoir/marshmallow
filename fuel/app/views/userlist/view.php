<h2><?=$user->fullname?></h2>

<?php if ($user->image) { echo "<img src=\"" . Uri::create("usericon/" . $user->username . '/' . $user->image). "\" width=100 height=100>"; } ?>
<br><br>

<table class="table table-striped">
    <tr><td>氏名</td><td><?=$user->fullname?></td></tr>
    <tr><td>プロフィール</td><td><?=$user->description?></td></tr>
    <tr><td>ユーザーID</td><td><?=$user->username?></td></tr>
    <tr><td>メールアドレス</td><td><?=$user->email?></td></tr>
    <tr><td>グループ</td><td><?=$user->group?></td></tr>
    <tr><td>最終ログイン日時</td><td><?=$user->last_login?></td></tr>
    <tr><td>登録日時</td><td><?=$user->created_at?></td></tr>
    <tr><td>更新日時</td><td><?=$user->updated_at?></td><tr>
</table>