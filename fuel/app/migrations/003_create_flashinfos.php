<?php

namespace Fuel\Migrations;

class Create_flashinfos
{
	public function up()
	{
		\DBUtil::create_table('flashinfos', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'subject' => array('constraint' => 50, 'type' => 'varchar'),
			'content' => array('type' => 'text'),
			'created_at' => array('type' => 'datetime', 'null' => true),
			'updated_at' => array('type' => 'datetime', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('flashinfos');
	}
}