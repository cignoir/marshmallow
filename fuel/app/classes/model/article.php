<?php
use Orm\Model;

class Model_Article extends Model
{
    protected static $_properties = array(
        'id',
        'username',
        'subject',
        'content',
        'created_at',
        'updated_at',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
                'events' => array('before_insert'),
                'mysql_timestamp' => true,
        ),
        'Orm\Observer_UpdatedAt' => array(
                'events' => array('before_save'),
                'mysql_timestamp' => true,
        ),
    );

    public static function validate($factory)
    {
        $val = Validation::forge($factory);
        $val->add_field('subject', 'Subject', 'max_length[50]');
        $val->add_field('content', 'Content', 'required');

        return $val;
    }
    
    protected static $_belongs_to = array(
        'user' => array(
            'model_to' => 'Model_User',
            'key_from' => 'username',
            'key_to' => 'username',
            'cascade_save' => false,
            'cascade_delete' => false
        )
    );
}
