<?php
use Orm\Model;

class Model_User extends Model
{
    protected static $_properties = array(
        'id',
        'username',
        'fullname',
        'password',
        'group',
        'email',
        'last_login',
        'login_hash',
        'profile_fields',
        'description',
        'image',
        'created_at',
        'updated_at',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
                'events' => array('before_insert'),
                'mysql_timestamp' => true,
        ),
        'Orm\Observer_UpdatedAt' => array(
                'events' => array('before_save'),
                'mysql_timestamp' => true,
        ),
    );

    public static function validate($factory)
    {
        $val = Validation::forge($factory);
        $val->add_field('username', 'Username', 'required|max_length[50]');
        $val->add_field('fullname', 'Fullname', 'max_length[50]');
        $val->add_field('password', 'Password', 'max_length[255]');
        $val->add_field('group', 'Group', 'valid_string[numeric]');
        $val->add_field('email', 'Email', 'valid_email|max_length[255]');
        $val->add_field('login_hash', 'Login Hash', 'max_length[255]');
        $val->add_field('profile_fields', 'Profile Fields', '');

        return $val;
    }

    protected static $_has_many = array(
        'articles' => array(
            'model_to' => 'Model_Article',
            'key_from' => 'username',
            'key_to' => 'username',
            'cascade_save' => false,
            'cascade_delete' => false
        )
    );
}
