<?php
class Controller_Home extends Controller_Template{
    public function before(){
        parent::before();

        if (Auth::check()) {
        } else {
            // 未ログイン時はログインページへリダイレクト
            Response::redirect('login/index');
        }
    }

    public function action_index(){
        $data = array();
        $data['me'] = Model_User::find(Auth::get_user_id()[1]);
        
        $data['users'] = Model_User::find('all', array('order_by' => array('created_at' => 'desc'), 'limit' => 3));
        $data['news'] = Model_Flashinfo::find('all', array('order_by' => array('created_at' => 'desc'), 'limit' => 2));
        $data['articles'] = Model_Article::find('all'
            , array(
                'related' => array('user'),
                'order_by' => array('created_at' => 'desc'),
                'limit' => 3
            )
        );
        $this->template->title = "ダッシュボード";
        $this->template->content = View::forge('home/index', $data);
    }
}