<?php
class Controller_Logout extends Controller_Template
{
    public function action_index()
    {
        Auth::logout();

        $this->template->title = "おつかれさまでした";
        $this->template->content = View::forge('logout/index');
    }
}