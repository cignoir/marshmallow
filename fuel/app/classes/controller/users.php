<?php
class Controller_Users extends Controller_Template{
    public function before(){
        parent::before();
        Config::load('upload', true);

//        if (true && Auth::check()) {
//        } else {
//            // 未ログイン時はログインページへリダイレクト
//            Response::redirect('login/index');
//        }
    }
    
    public function action_index()
    {
        $data['me'] = Model_User::find(Auth::get_user_id()[1]);
        $data['users'] = Model_User::find('all');
        $this->template->title = "社員管理";
        $this->template->content = View::forge('users/index', $data);
    }

    public function action_view($id = null)
    {
        is_null($id) and Response::redirect('users');

        if ( ! $data['user'] = Model_User::find($id))
        {
                Session::set_flash('error', 'データが見つかりませんでした #'.$id);
                Response::redirect('users');
        }

        $this->template->title = "社員管理";
        $this->template->content = View::forge('users/view', $data);

    }

    public function action_create()
    {
        if (Input::method() == 'POST')
        {
            $val = Model_User::validate('create');

            if ($val->run())
            {
                $result = Auth::create_user(Input::post('username'),  Input::post('password'),  Input::post('email'), 1, array(''));

                if ($result)
                {
                    $user = Model_User::find('last');

                    if($user){
                        $user['fullname'] = Input::post('fullname');
                        $user['description'] = Input::post('description');

                        $now = date('c');
                        $user['last_login'] = $now;
                        $user['created_at'] = $now;
                        $user['updated_at'] = $now;
                        
                        if(isset($_FILES["usericon"]) && isset($_FILES["usericon"]["name"])){
                            Upload::process(array('path' => Config::get('upload.path') . '/' . $user->username));
                            if (Upload::is_valid()) {
                                Upload::save();
                                $user->image =  $_FILES["usericon"]["name"];
                            }
                        }

                        if($user->save()){
                            Session::set_flash('success', '登録に成功しました #'.Input::post('username').'.');
                            Response::redirect('users');
                        }
                    }
                }
                else
                {
                    Session::set_flash('error', '正常にデータを保存できませんでした');
                }
            }
            else
            {
                Session::set_flash('error', $val->error());
            }
        }

        $this->template->title = "社員管理";
        $this->template->content = View::forge('users/create');
    }

    public function action_edit($id = null)
    {
        is_null($id) and Response::redirect('users');

        if ( ! $user = Model_User::find($id))
        {
            Session::set_flash('error', 'Could not find user #'.$id);
            Response::redirect('users');
        }

        $val = Model_User::validate('edit');

        if ($val->run())
        {
            $user->fullname = Input::post('fullname');
            $user->email = Input::post('email');
            $user->description = Input::post('description');
            
            if(Input::post('group')){
                $user->group = Input::post('group');
            }
            
            if(isset($_FILES["usericon"]) && isset($_FILES["usericon"]["name"])){
                Upload::process(array('path' => Config::get('upload.path')));
                if (Upload::is_valid()) {
                    Upload::save();
                    $user->image =  $_FILES["usericon"]["name"];
                }
            }
            
            if ($user->save())
            {
                    Session::set_flash('success', '更新に成功しました #' . $id);
                    Response::redirect('users');
            }
            else
            {
                    Session::set_flash('error', '更新に失敗しました #' . $id);
            }
        }

        else
        {
            if (Input::method() == 'POST')
            {
                $user->fullname = Input::post('fullname');
                $user->group = $val->validated('group');
                $user->email = $val->validated('email');
                $user->description = Input::post('description');

                Session::set_flash('error', $val->error());
            }

            $this->template->set_global('user', $user, false);
        }

        $this->template->title = "社員管理";
        $this->template->content = View::forge('users/edit');

    }

    public function action_delete($id = null)
    {
        is_null($id) and Response::redirect('users');

        if ($user = Model_User::find($id))
        {
            $user->delete();

            Session::set_flash('success', '削除に成功しました #'.$id);
        }

        else
        {
            Session::set_flash('error', '削除に失敗しました #'.$id);
        }

        Response::redirect('users');

    }
}
