<?php
class Controller_Articles extends Controller_Template{
    public function before(){
        parent::before();
        
        if (Auth::check()) {
        } else {
            // 未ログイン時はログインページへリダイレクト
            Response::redirect('login/index');
        }
    }

    public function action_index()
    {
        $data['articles'] = Model_Article::find('all'
            , array(
                'related' => array('user'),
                'order_by' => array('created_at' => 'desc')
            )
        );
        $data['me'] = Model_User::find(Auth::get_user_id()[1]);
        $this->template->title = '記事一覧';
        $this->template->content = View::forge('articles/index', $data);

    }

    public function action_view($id = null)
    {
            is_null($id) and Response::redirect('articles');

            if ( ! $data['article'] = Model_Article::find($id, array('related' => array('user'))))
            {
                    Session::set_flash('error', 'Could not find article #'.$id);
                    Response::redirect('articles');
            }

            $this->template->title = "記事閲覧";
            $this->template->content = View::forge('articles/view', $data);

    }

    public function action_create()
    {
        if (Input::method() == 'POST')
        {
            $val = Model_Article::validate('create');

            if ($val->run())
            {
                $user = Model_User::find(Auth::get_user_id()[1]);
                $article = Model_Article::forge(array(
                        'username' => $user->username,
                        'subject' => Input::post('subject'),
                        'content' => Input::post('content'),
                ));

                if ($article and $article->save())
                {
                        Session::set_flash('success', '記事の投稿しました');

                        Response::redirect('articles');
                }

                else
                {
                        Session::set_flash('error', '記事の投稿に失敗しました');
                }
            }
            else
            {
                    Session::set_flash('error', $val->error());
            }
        }

        $this->template->title = "記事 新規投稿";
        $this->template->content = View::forge('articles/create');
    }

    public function action_edit($id = null)
    {
        is_null($id) and Response::redirect('articles');

        if ( ! $article = Model_Article::find($id))
        {
            Session::set_flash('error', 'Could not find article #'.$id);
            Response::redirect('articles');
        }

        $val = Model_Article::validate('edit');

        if ($val->run())
        {
            $user = Model_User::find(Auth::get_user_id()[1]);
            $article->username =  $user->username;
            $article->subject = Input::post('subject');
            $article->content = Input::post('content');

            if ($article->save())
            {
                    Session::set_flash('success', 'Updated article #' . $id);

                    Response::redirect('articles');
            }

            else
            {
                    Session::set_flash('error', 'Could not update article #' . $id);
            }
        }

        else
        {
            if (Input::method() == 'POST')
            {
                $user = Model_User::find(Auth::get_user_id()[1]);
                $article->username = $user->username;
                $article->subject = $val->validated('subject');
                $article->content = $val->validated('content');

                Session::set_flash('error', $val->error());
            }

            $this->template->set_global('article', $article, false);
        }

        $this->template->title = "記事 編集";
        $this->template->content = View::forge('articles/edit');

    }

    public function action_delete($id = null)
    {
        is_null($id) and Response::redirect('articles');

        if ($article = Model_Article::find($id))
        {
            $article->delete();

            Session::set_flash('success', 'Deleted article #'.$id);
        }

        else
        {
            Session::set_flash('error', 'Could not delete article #'.$id);
        }

        Response::redirect('articles');
    }
}
