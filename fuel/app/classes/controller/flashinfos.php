<?php
class Controller_Flashinfos extends Controller_Template{

	public function action_index()
	{
		$data['flashinfos'] = Model_Flashinfo::find('all');
		$this->template->title = "ニュース管理";
		$this->template->content = View::forge('flashinfos/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('flashinfos');

		if ( ! $data['flashinfo'] = Model_Flashinfo::find($id))
		{
			Session::set_flash('error', 'Could not find flashinfo #'.$id);
			Response::redirect('flashinfos');
		}

		$this->template->title = "ニュース管理";
		$this->template->content = View::forge('flashinfos/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Flashinfo::validate('create');
			
			if ($val->run())
			{
				$flashinfo = Model_Flashinfo::forge(array(
					'subject' => Input::post('subject'),
					'content' => Input::post('content'),
				));

				if ($flashinfo and $flashinfo->save())
				{
					Session::set_flash('success', 'Added flashinfo #'.$flashinfo->id.'.');

					Response::redirect('flashinfos');
				}

				else
				{
					Session::set_flash('error', 'Could not save flashinfo.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "ニュース管理";
		$this->template->content = View::forge('flashinfos/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('flashinfos');

		if ( ! $flashinfo = Model_Flashinfo::find($id))
		{
			Session::set_flash('error', 'Could not find flashinfo #'.$id);
			Response::redirect('flashinfos');
		}

		$val = Model_Flashinfo::validate('edit');

		if ($val->run())
		{
			$flashinfo->subject = Input::post('subject');
			$flashinfo->content = Input::post('content');

			if ($flashinfo->save())
			{
				Session::set_flash('success', 'Updated flashinfo #' . $id);

				Response::redirect('flashinfos');
			}

			else
			{
				Session::set_flash('error', 'Could not update flashinfo #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$flashinfo->subject = $val->validated('subject');
				$flashinfo->content = $val->validated('content');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('flashinfo', $flashinfo, false);
		}

		$this->template->title = "ニュース管理";
		$this->template->content = View::forge('flashinfos/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('flashinfos');

		if ($flashinfo = Model_Flashinfo::find($id))
		{
			$flashinfo->delete();

			Session::set_flash('success', 'Deleted flashinfo #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete flashinfo #'.$id);
		}

		Response::redirect('flashinfos');

	}


}
