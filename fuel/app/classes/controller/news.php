<?php
class Controller_News extends Controller_Template{

    public function action_index()
    {
        $data['news'] = Model_Flashinfo::find('all', array('order_by' => array('created_at' => 'desc')));
        $this->template->title = "ニュース";
        $this->template->content = View::forge('news/index', $data);
    }
}
