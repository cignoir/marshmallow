<?php
class Controller_Userlist extends Controller_Template{
    public function before(){
        parent::before();

        if (true && Auth::check()) {
        } else {
            // 未ログイン時はログインページへリダイレクト
            Response::redirect('login/index');
        }
    }
    
    public function action_index()
    {
        
        $data['users'] = Model_User::find('all');
        $this->template->title = "社員一覧";
        $this->template->content = View::forge('userlist/index', $data);
    }
    
    public function action_view($id = null)
    {
        is_null($id) and Response::redirect('userlist');

        if ( ! $data['user'] = Model_User::find($id))
        {
                Session::set_flash('error', 'データが見つかりませんでした #'.$id);
                Response::redirect('userlist');
        }

        $this->template->title = "社員閲覧";
        $this->template->content = View::forge('userlist/view', $data);
    }
    
    public function action_edit($id = null)
    {
        is_null($id) and Response::redirect('userlist');

        if ( ! $user = Model_User::find($id))
        {
            Session::set_flash('error', 'Could not find user #'.$id);
            Response::redirect('userlist');
        }

        $login_user_id_tmp = Auth::get_user_id();
        $login_user_id = $login_user_id_tmp[1];
        if($user->id != $login_user_id){
            Session::set_flash('error', '編集権限がありません');
            Response::redirect('userlist');
        }
        
        $val = Model_User::validate('edit');

        if ($val->run())
        {
            $user->fullname = Input::post('fullname');
            $user->email = Input::post('email');
            $user->description = Input::post('description');
            
            if(Input::post('group')){
                $user->group = Input::post('group');
            }
            
            if(isset($_FILES["usericon"]) && isset($_FILES["usericon"]["name"])){
                Upload::process(array('path' => Config::get('upload.path') . '/' . $user->username));
                if (Upload::is_valid()) {
                    Upload::save();
                    $user->image =  $_FILES["usericon"]["name"];
                }
            }
            
            if ($user->save())
            {
                    Session::set_flash('success', '更新に成功しました #' . $id);
                    Response::redirect('userlist');
            }
            else
            {
                    Session::set_flash('error', '更新に失敗しました #' . $id);
            }
        }
        else
        {
            if (Input::method() == 'POST')
            {
                $user->fullname = Input::post('fullname');
                $user->group = $val->validated('group');
                $user->email = $val->validated('email');
                $user->description = Input::post('description');

                Session::set_flash('error', $val->error());
            }

            $this->template->set_global('user', $user, false);
        }

        $this->template->title = "編集";
        $this->template->content = View::forge('userlist/edit');

    }
}
